package com.scalefocus.parts;

public abstract class AbstractPart {

    private int constructionTime;
    private boolean isBuilt;

    public AbstractPart(int constructionTime) {
        this.setConstructionTime(constructionTime);
        this.setBuilt(false);
    }

    public int getConstructionTime() {
        return constructionTime;
    }


    public void setConstructionTime(int constructionTime) {
        this.constructionTime = constructionTime;
    }

    public boolean isBuilt() {
        return isBuilt;
    }

    public void setBuilt(boolean built) {
        isBuilt = built;
    }
}
