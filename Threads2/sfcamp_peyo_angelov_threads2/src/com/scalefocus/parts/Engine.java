package com.scalefocus.parts;

public class Engine extends AbstractPart {

    private static final int CONSTRUCTION_TIME = 7000;

    public Engine() {
        super(CONSTRUCTION_TIME);
    }

}
