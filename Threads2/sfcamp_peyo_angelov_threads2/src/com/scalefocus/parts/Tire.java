package com.scalefocus.parts;

public class Tire extends AbstractPart{

    private static final int CONSTRUCTION_TIME = 2000;

    public Tire() {
        super(CONSTRUCTION_TIME);
    }

}
