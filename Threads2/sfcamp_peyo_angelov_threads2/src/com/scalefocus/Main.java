package com.scalefocus;

import com.scalefocus.parts.*;
import com.scalefocus.threads.WorkerThread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    public static void main(String[] args) {

        List<AbstractPart> parts = new ArrayList<>();

        AbstractPart seat1 = new Seat();
        AbstractPart seat2 = new Seat();
        AbstractPart seat3 = new Seat();
        AbstractPart seat4 = new Seat();
        AbstractPart seat5 = new Seat();

        AbstractPart tire1 = new Tire();
        AbstractPart tire2 = new Tire();
        AbstractPart tire3 = new Tire();
        AbstractPart tire4 = new Tire();

        AbstractPart engine = new Engine();
        AbstractPart frame = new Frame();

        /**
         * Engine = 7 secs  x1
         * Frame = 5 secs x1
         * Seat = 3 secs x5
         * Tire = 2 secs x4
         */
        parts.add(engine);
        parts.add(frame);

        parts.add(tire1);
        parts.add(tire2);
        parts.add(tire3);
        parts.add(tire4);

        parts.add(seat1);
        parts.add(seat2);
        parts.add(seat3);
        parts.add(seat4);
        parts.add(seat5);


        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Long time = 0L;
        List<Future<Long>> times = new ArrayList<>();
        for (AbstractPart part : parts) {

            times.add(executorService.submit(new WorkerThread(part)));
        }


        for (Future f: times ) {
            try {
                time+= (Long)f.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        executorService.shutdown();

        System.out.println("That took " + time + " milliseconds.");

    }
}
