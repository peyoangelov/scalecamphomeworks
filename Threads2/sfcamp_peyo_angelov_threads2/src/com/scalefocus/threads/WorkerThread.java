package com.scalefocus.threads;

import com.scalefocus.parts.AbstractPart;

import java.util.concurrent.Callable;

public class WorkerThread implements Callable<Long> {
    private AbstractPart part;

    public WorkerThread(AbstractPart part) {
        this.part = part;
    }

    @Override
    public Long call() {
        long startTime = System.currentTimeMillis();
        System.out.println("Building " + part.getClass() + " on thread: " + Thread.currentThread().getId());
        try {
            Thread.sleep(part.getConstructionTime());
        } catch (InterruptedException e) {
            Thread.interrupted();
        }
        System.out.println("Done Building " + part.getClass() + " on thread: " + Thread.currentThread().getId());
        long endTime = System.currentTimeMillis();

        return endTime - startTime;
    }
}
