package com.scalefocus.services;

public interface GreetingRepository {

  String getEnglishGreeting();

  String getSpanishGreeting();

  String getGermanGreeting();
}
