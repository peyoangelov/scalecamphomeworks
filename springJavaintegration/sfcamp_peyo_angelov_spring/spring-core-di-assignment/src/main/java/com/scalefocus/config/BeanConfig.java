package com.scalefocus.config;

import com.scalefocus.controllers.ConstructorInjectedController;
import com.scalefocus.controllers.GetterInjectedController;
import com.scalefocus.controllers.MyController;
import com.scalefocus.controllers.PropertyInjectedController;
import com.scalefocus.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
public class BeanConfig {
    @Autowired
    GreetingService greetingService;

    @Bean
    public ConstructorInjectedController constructorInjectedController() {
        return new ConstructorInjectedController(constructorGreetingService());
    }

    @Bean
    public MyController myController(GreetingService greetingService) {

        return new MyController(greetingService);
    }

    @Bean
    public PropertyInjectedController propertyInjectedController() {
        return new PropertyInjectedController();
    }

    @Bean
    public GetterInjectedController getterInjectedController() {
        GetterInjectedController getterInjectedController = new GetterInjectedController();
        getterInjectedController.setGreetingService(getterGreetingService());
        return getterInjectedController;
    }

    @Bean
    public GreetingService greetingServiceImpl() {
        return new GreetingServiceImpl();
    }

    @Bean
    public GetterGreetingService getterGreetingService() {
        return new GetterGreetingService();
    }

    @Bean
    public ConstructorGreetingService constructorGreetingService() {
        return new ConstructorGreetingService();
    }

    @Bean
    public GreetingRepository greetingRepository() {
        return new GreetingRepositoryImpl();
    }

    @Bean
    @Profile("es")
    @Primary
    public PrimarySpanishGreetingService primarySpanishGreetingService(GreetingRepository greetingRepository){
        return new PrimarySpanishGreetingService(greetingRepository);
    }

    @Bean
    @Profile("de")
    @Primary
    public PrimaryGermanGreetingService primaryGermanGreetingService (GreetingRepository greetingRepository){
        return new PrimaryGermanGreetingService(greetingRepository);
    }

    @Bean
    @Profile({"en", "default"})
    @Primary
    public PrimaryGreetingService primaryGreetingService(GreetingRepository greetingRepository) {
return new PrimaryGreetingService(greetingRepository);
    }

}
