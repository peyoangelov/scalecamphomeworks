package com.scalefocus.controllers;

import com.scalefocus.services.GreetingService;
import com.scalefocus.services.GreetingServiceImpl;


public class PropertyInjectedController {

    public GreetingService greetingServiceImpl = new GreetingServiceImpl();

    public String sayHello() {
        return greetingServiceImpl.sayGreeting();
    }

}
