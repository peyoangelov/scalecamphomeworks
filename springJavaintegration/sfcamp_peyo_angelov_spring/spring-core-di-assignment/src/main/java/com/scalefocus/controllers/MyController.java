package com.scalefocus.controllers;

import com.scalefocus.services.GreetingService;


public class MyController {

    private GreetingService greetingService;

    public MyController(GreetingService greetingService) {

        this.greetingService = greetingService;
    }

    public String hello() {
        System.out.println("Hello!!! ");

        return greetingService.sayGreeting();
    }
}
