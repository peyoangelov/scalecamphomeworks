package com.scalefocus.controllers;

import com.scalefocus.exceptions.BadRequestException;
import com.scalefocus.exceptions.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@ControllerAdvice
public class ControllerExceptionHandler {
    private final static String ERROR_CODE = "errorCode";


    @ExceptionHandler(value = BadRequestException.class)
    public ModelAndView handleBadRequestException(final Exception exception) {
        log.error("Handling bad Request exception");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error-page");
        modelAndView.addObject("exception", exception);

        modelAndView.addObject(ERROR_CODE, HttpStatus.BAD_REQUEST.value());


        return modelAndView;
    }

    @ExceptionHandler(value = NotFoundException.class)
    public ModelAndView handleNotFoundException(final Exception exception) {
        log.error("Handling not found exception");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("404-error-page");
        modelAndView.addObject("exception", exception);

        return modelAndView;
    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView handleGeneralException(final Exception exception) {

        log.error("Handling not found exception");

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("error-page");
        modelAndView.addObject("exception", exception);
        modelAndView.addObject(ERROR_CODE, "404");
        return modelAndView;
    }
}
