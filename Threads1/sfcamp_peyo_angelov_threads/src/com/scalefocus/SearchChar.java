package com.scalefocus;

import java.util.List;
import java.util.concurrent.Callable;

public class SearchChar implements Callable<Integer> {

    private List<String> words;


    public SearchChar(List<String> words) {
        this.words = words;

    }

    public Integer call() {
        return readFile();
    }

    /**
     * Reading the file
     *
     * @return
     */
    public int readFile() {

        int count = 0;
        for (String word : this.words) {
            count += letterCount(word);
        }
        return count;
    }

    /**
     * Counts the letter A
     *
     * @param text
     * @return
     */
    private static int letterCount(final String text) {
        int counter = 0;

        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == 'a' || text.charAt(i) == 'A') {
                counter++;
            }
        }
        return counter;
    }


}

