package com.scalefocus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {

        Path inputFile = Paths.get("src/com/scalefocus/warAndPeace.txt");

        try {
            List<String> words = Files.readAllLines(inputFile);
            ExecutorService exec = Executors.newFixedThreadPool(1);
            SearchChar searchChar = new SearchChar(words);

            long startTime = System.currentTimeMillis();
            Future<Integer> future = exec.submit(searchChar);
            Integer result = future.get();
            System.out.println("==================");
            System.out.println("With one thread:\nNumber of 'a' chars:  " + result);
            long endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds.");
            System.out.println("==================");

            exec.shutdown();


            /**
             * Logic for 10 threads
              */
            int wordsPerThread = words.size() / 10;
            Collection<Callable<Integer>> tasks = new ArrayList();
            ExecutorService executorService = Executors.newFixedThreadPool(10);


            for (int i = 0; i < words.size(); i += wordsPerThread) {
                SearchChar searchChar1 = new SearchChar(words.subList(i, i + wordsPerThread));
                tasks.add(searchChar1);
            }
            startTime = System.currentTimeMillis();
            List<Future<Integer>> results = executorService.invokeAll(tasks);

            int counter = 0;
            for (Future f : results) {
                counter += (Integer) f.get();
            }
            System.out.println("==================");
            System.out.println("With 10 threads:\nNumber of 'a' chars:  " + counter);

            endTime = System.currentTimeMillis();
            System.out.println("That took " + (endTime - startTime) + " milliseconds.");
            System.out.println("==================");
            executorService.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}

