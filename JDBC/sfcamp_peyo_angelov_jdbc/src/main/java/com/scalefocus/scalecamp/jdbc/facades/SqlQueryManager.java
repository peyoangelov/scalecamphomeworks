package com.scalefocus.scalecamp.jdbc.facades;

import com.scalefocus.scalecamp.jdbc.services.DDLService;
import com.scalefocus.scalecamp.jdbc.services.MetadataReaderService;
import com.scalefocus.scalecamp.jdbc.services.StoredProceduresService;
import com.scalefocus.scalecamp.jdbc.BaseJdbc;
import com.scalefocus.scalecamp.jdbc.services.DMLService;

import java.util.List;
import java.util.Map;


public class SqlQueryManager {
    private final DMLService dmlService = new DMLService();
    private final DDLService ddlService = new DDLService();
    private final StoredProceduresService storedProceduresService = new StoredProceduresService();
    private final BaseJdbc baseJdbc = new BaseJdbc();
    private final MetadataReaderService metadataReaderService = new MetadataReaderService();


    public List<String> getAllAvailableProcedures() {
        return storedProceduresService.getAllAvailableStoredProcedures();
    }

    public void manageQuery(final String query) {
        if (isDMLSelect(query)) {
            dmlService.getInformationFromDataBase(query);
        } else if (isDMLModification(query)) {
            dmlService.modifyInformationFromDataBase(query);
        } else if (isDDLOperation(query)) {
            ddlService.modifyDataBaseStructure(query);
        } else if (isExceptionInfoCalled(query)) {
            baseJdbc.getLastExceptionStackTrace();
        } else if (isMetadataInfoCalled(query)){
            metadataReaderService.getMetadataInfo(query);
        }
    }

    public void manageStoredProcedure(final String procedure, final Map<String, String> arguments) {
        storedProceduresService.executeStoredProcedure(procedure, arguments);

    }

    private boolean isDMLSelect(final String query) {
        return query.toUpperCase().startsWith("SELECT");
    }

    private boolean isDMLModification(final String query) {
        return query.toUpperCase().startsWith("UPDATE") || query.toUpperCase().startsWith("DELETE") || query.toUpperCase().startsWith("INSERT");
    }

    private boolean isDDLOperation(final String query) {
        return query.toUpperCase().startsWith("CREATE") || query.toUpperCase().startsWith("DROP");

    }

    public boolean isStoredProcedure(final String query) {
        return query.toUpperCase().startsWith("CALL");
    }

    public boolean doesProcedureExist(List<String> procedures, String query) {

        for (String name : procedures) {
            if (query.toUpperCase().replace("CALL", "").replace("?", "").replace(",", "").replace("(", "").replace(")", "").trim().equalsIgnoreCase(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean isExceptionInfoCalled(final String query) {
        return query.toLowerCase().startsWith("stacktrace");
    }

    public boolean isQuitCalled(final String query) {
        return query.toLowerCase().startsWith("quit");
    }
    private boolean isMetadataInfoCalled(final String query) {
        return query.toLowerCase().startsWith("metadata");
    }

}

