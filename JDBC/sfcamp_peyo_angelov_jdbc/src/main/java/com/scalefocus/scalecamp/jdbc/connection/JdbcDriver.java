package com.scalefocus.scalecamp.jdbc.connection;

import com.scalefocus.scalecamp.jdbc.BaseJdbc;

import java.lang.invoke.MethodHandles;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class JdbcDriver extends BaseJdbc {
    static BaseJdbc baseJdbc = new BaseJdbc();
    public static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
//    public static  String DATABASE_URL = "jdbc:oracle:thin:@localhost:1521:XE";
//    public static  String DATABASE_USER = "hr";
//    public static  String DATABASE_PASSWORD = "1234";
    public static String DATABASE_URL = "";
    public static String DATABASE_USER = "";
    public static String DATABASE_PASSWORD = "";

    public static void main(String... args) {
        try (Connection connection = JdbcDriver.getConnection()) {
            System.out.println("Main is closed: " + connection.isClosed());
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);

        }
    }


    public static Connection getConnection() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD);

        } catch (SQLException e) {
            baseJdbc.logSqlException(e);
        }
        return connection;
    }

}
