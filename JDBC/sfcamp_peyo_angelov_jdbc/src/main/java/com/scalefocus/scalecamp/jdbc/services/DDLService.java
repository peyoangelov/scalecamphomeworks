package com.scalefocus.scalecamp.jdbc.services;

import com.scalefocus.scalecamp.jdbc.BaseJdbc;
import com.scalefocus.scalecamp.jdbc.connection.JdbcDriver;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DDLService {
    BaseJdbc baseJdbc = new BaseJdbc();
    /**
     * For Create / drop tables
     *
     * @param userInput
     */

    public void modifyDataBaseStructure(final String userInput) {

        try (Connection connection = JdbcDriver.getConnection();
             Statement statement = connection.createStatement()) {
            statement.executeUpdate(userInput);

            System.out.println("Successful operation.");
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);
            System.out.println("Operation Unsuccessful! Reason:  " + e.getMessage());
        }

    }
}
