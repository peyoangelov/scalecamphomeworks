package com.scalefocus.scalecamp.jdbc.services;

import com.scalefocus.scalecamp.jdbc.BaseJdbc;
import com.scalefocus.scalecamp.jdbc.connection.JdbcDriver;

import java.sql.*;
import java.util.*;

public class StoredProceduresService {
    BaseJdbc baseJdbc = new BaseJdbc();

    /**
     * Gets all availavle stored procedures
     * @return List<String> all procedures
     */
    public List<String> getAllAvailableStoredProcedures() {

        List<String> procedures = new ArrayList<>();
        try (Connection connection = JdbcDriver.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("select * from all_objects where object_type = 'PROCEDURE' and OWNER = 'HR'");
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                procedures.add(resultSet.getString(2));
            }
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);

        }
        return procedures;
    }

    /**
     * Getting the names of argument for stored procedure
     * @param procedure
     * @return List<String> arguments
     */
    public List<String> getNamesOfArgumentForStoredProcedure(final String procedure) {

        List<String> arguments = new ArrayList<>();
        try (Connection connection = JdbcDriver.getConnection();
             Statement statement = connection.createStatement()) {
       
            statement.execute("select argument_name from user_arguments where OBJECT_NAME = '" + procedure.replace("?","").replace(",","").trim() + "'");

            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                arguments.add(resultSet.getString(1));
            }
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);

        }
        Collections.reverse(arguments);
        return arguments;

    }

    /**
     * executing the stored procedure
     * @param procedure
     * @param arguments
     */
    public void executeStoredProcedure(final String procedure, final Map<String, String> arguments) {
        String modifiedProcedureCall = modifyProcedureCall(arguments.size());
       String procedureCall = "{" + procedure + modifiedProcedureCall+ "}";
     //   String procedureCall = "{" + procedure + "}";
        try (Connection connection = JdbcDriver.getConnection();
             CallableStatement statement = connection.prepareCall(procedureCall)) {

            for (int i = 0; i < arguments.size(); i++) {

                statement.setString(i+1, arguments.get(i));

            }

            statement.execute();
            System.out.println("Done.");
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);

        }
    }

    /**
     * If the userinput is only  'call <name_of_procedure>' here is added '(?,?)'depends on how much are arguments
     * @param numberOfArguments
     * @return
     */
    private String modifyProcedureCall(final int numberOfArguments){
        StringBuilder modifiedProcedureCall = new StringBuilder();
        modifiedProcedureCall.append("(");
        for (int i =0 ; i < numberOfArguments; i++){
            modifiedProcedureCall.append("?").append(",");
        }
        modifiedProcedureCall.deleteCharAt(modifiedProcedureCall.lastIndexOf(","));
        modifiedProcedureCall.append(")");
        return modifiedProcedureCall.toString();
    }
}
