package com.scalefocus.scalecamp.jdbc.services;

import com.scalefocus.scalecamp.jdbc.connection.JdbcDriver;
import com.scalefocus.scalecamp.jdbc.BaseJdbc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;

public class MetadataReaderService {

    public void getMetadataInfo(final String userInput) {
        try (Connection connection = JdbcDriver.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();

            String[] tokens = userInput.split("\\s+");
            if (tokens.length!=2){
                System.out.println("Wrong number of parameters!");
                return;
            }

            String parameter = tokens[1].toLowerCase();

            for (Method method : DatabaseMetaData.class.getMethods()) {
                if (method.getParameterCount() == 0
                        && (method.getName().startsWith("get") || method.getName().startsWith("support"))
                        && method.getName().toLowerCase().contains(parameter)) {

                    System.out.printf("%-50s %s \n", method.getName(), method.invoke(metaData, new Object[]{}));
                }
            }


        } catch (SQLException e) {
            BaseJdbc.logSqlException(e);
            System.out.println("Operation Unsuccessful! Reason:  " + e.getMessage());
        } catch (IllegalAccessException e) {
            e.getMessage();
        } catch (InvocationTargetException e) {
            e.getMessage();
        }
    }
}
