package com.scalefocus.scalecamp.jdbc;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.logging.Logger;

public class BaseJdbc {
    protected static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
    static Deque<String> exceptions = new ArrayDeque<>();

    public static void logSqlException(SQLException e) {

        logger.severe("Message: " + e.getMessage());
        logger.severe("SQL State: " + e.getSQLState());
        logger.severe("SQL Code: " + e.getErrorCode());
        logger.severe("Next Exception: " + e.getNextException());
        logger.severe("Stacktrace: " + e.getStackTrace());
        StringWriter writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        printWriter.flush();
        exceptions.push(writer.toString());

    }

    public static void getLastExceptionStackTrace() {
        if (exceptions.isEmpty()) {
            logger.severe("No Exception");
        }

        logger.severe(exceptions.pop());
    }


}
