package com.scalefocus.scalecamp.jdbc.connection;

import com.scalefocus.scalecamp.jdbc.services.StoredProceduresService;
import com.scalefocus.scalecamp.jdbc.facades.SqlQueryManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static final SqlQueryManager sqlQueryManager = new SqlQueryManager();
    private static final StoredProceduresService storedProceduresService = new StoredProceduresService();
    public static String userInput = "";

    public static void main(String[] args) {
        Map<String, String> arguments = new HashMap<>();

        JdbcDriver.DATABASE_URL = userInput("Insert URL fro DB");
        JdbcDriver.DATABASE_USER = userInput("Insert USER");
        JdbcDriver.DATABASE_PASSWORD = userInput("Insert Password");
        while (!sqlQueryManager.isQuitCalled(userInput)) {

            System.out.println("For DML or DDL just insert the query.\nTo get information for METADATA insert 'metadata' followed by the name of the param.\nTo EXIT insert 'QUIT' ");
            System.out.println(("To see the stacktrace of the latest SQL exception insert'stacktrace'\nFor stored procedures insert 'CALL'  followed by the name of the procedure.\nAvailable procedures are : "));
            List<String> procedures = sqlQueryManager.getAllAvailableProcedures();
            for (String name : procedures) {
                System.out.println((name));

            }


            userInput = userInput("Insert query.");
            if (sqlQueryManager.isStoredProcedure(userInput)) {
                while (!sqlQueryManager.doesProcedureExist(procedures, userInput)) {

                    userInput = userInput("Insert existing procedure!");
                }


                System.out.println("Available arguments:");
                List<String> getAllArgumentsFromDb = storedProceduresService.getNamesOfArgumentForStoredProcedure(userInput.toUpperCase().replace("CALL", "").replace("?", "").replace(",", "").replace("(", "").replace(")", "").trim());

                for (String arg : getAllArgumentsFromDb) {

                    arguments.put(arg, userInput("Insert value for argument " + arg));

                }
                sqlQueryManager.manageStoredProcedure(userInput, arguments);

            } else {
                sqlQueryManager.manageQuery(userInput);
            }
        }
    }


    private static String userInput(String message) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println((message));
        String s = null;
        try {
            s = br.readLine();
        } catch (IOException e) {
            e.getMessage();

        }

        return s;
    }
}



