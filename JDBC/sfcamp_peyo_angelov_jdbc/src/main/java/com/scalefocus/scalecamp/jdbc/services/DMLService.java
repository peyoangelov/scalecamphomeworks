package com.scalefocus.scalecamp.jdbc.services;

import com.scalefocus.scalecamp.jdbc.connection.JdbcDriver;
import com.scalefocus.scalecamp.jdbc.BaseJdbc;

import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class DMLService {
    protected static final Logger logger = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
    BaseJdbc baseJdbc = new BaseJdbc();
    public void getInformationFromDataBase(final String userInput) {

        try (Connection connection = JdbcDriver.getConnection();
             Statement statement = connection.createStatement()) {

            boolean result = statement.execute(userInput);
            int rowCounter = 0;
            if (result) {
                ResultSet resultSet = statement.getResultSet();
                ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();
                List<String> columns = new ArrayList<>();
                for (int i = 1; i <= columnCount; i++) {
                    columns.add(String.format("%-15.15s", metaData.getColumnName(i)));
                }

                System.out.println(String.join(" | ", columns));

                while (resultSet.next()) {

                    List<String> columnsRes = new ArrayList<>();
                    for (int i = 1; i <= columnCount; i++) {
                        columnsRes.add(String.format("%-15.15s", resultSet.getString(i)));

                    }
                    System.out.println(String.join(" | ", columnsRes));

                    rowCounter++;
                }

                System.out.println("\n Number of all records: " + rowCounter);
                resultSet.close();
            }
        } catch (SQLException e) {
            baseJdbc.logSqlException(e);
        }
    }

    public void modifyInformationFromDataBase(final String userInput) {

        try (Connection connection = JdbcDriver.getConnection();
             Statement statement = connection.createStatement()) {
            int result = statement.executeUpdate(userInput);
            if (result > 0) {
                System.out.println("The statement affected: " + result + "row(s).");
            } else {
                System.out.println("No rows were effected.");
            }

        } catch (SQLException e) {
            baseJdbc.logSqlException(e);
        }

    }
}



