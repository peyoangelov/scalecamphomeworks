package com.scalefocus.controllers.v1;

import com.scalefocus.api.v1.model.CustomerDTO;
import com.scalefocus.api.v1.model.VendorDTO;
import com.scalefocus.api.v1.model.VendorListDTO;
import com.scalefocus.services.VendorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(VendorController.BASE_URL)
public class VendorController {

    public static final String BASE_URL = "/api/v1/vendors";
    private final VendorService vendorService;

    public VendorController(VendorService vendorService) {
        this.vendorService = vendorService;
    }


    @DeleteMapping({"/{id}"})
    public ResponseEntity<Void> deleteVendor(@PathVariable Long id) {

        vendorService.deleteVendorById(id);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @PatchMapping({"/{id}"})
    public ResponseEntity<VendorDTO> patchVendor(@PathVariable Long id, @RequestBody VendorDTO vendorDTO) {
        return new ResponseEntity<VendorDTO>(vendorService.patchVendor(id, vendorDTO),
                HttpStatus.OK);
    }
    @PutMapping({"/{id}"})
    public ResponseEntity<VendorDTO> updateVendor(@PathVariable Long id, @RequestBody VendorDTO vendorDTO) {
        return new ResponseEntity<VendorDTO>(vendorService.saveVendorByDTO(id, vendorDTO),
                HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<VendorDTO> createNewVendor(@RequestBody VendorDTO vendorDTO) {
        return new ResponseEntity<VendorDTO>(vendorService.createNewVendor(vendorDTO),
                HttpStatus.CREATED);
    }



    @GetMapping({"/{id}"})
    public ResponseEntity<VendorDTO> getVendorById(@PathVariable final Long id) {

        return new ResponseEntity<VendorDTO>(vendorService.getVendorById(id),
                HttpStatus.OK);
    }
    @GetMapping
    public ResponseEntity<VendorListDTO> getAllvendors() {

        List<VendorDTO> vendors = vendorService.getAllVendors();
        VendorListDTO vendorListDTO = new VendorListDTO();
        vendorListDTO.setVendors(vendors);
        return new ResponseEntity<VendorListDTO>(vendorListDTO,
                HttpStatus.OK);
    }


}
