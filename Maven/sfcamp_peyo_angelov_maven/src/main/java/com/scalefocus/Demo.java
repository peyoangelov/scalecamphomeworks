package com.scalefocus;

import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Demo {
    private static org.apache.log4j.Logger LOGGER = Logger.getLogger(Demo.class);
    public static void main(String[] args) {
        Demo demo = new Demo();
        Properties dev = demo.loadPropertiesFile("config.properties");
        dev.forEach((k, v) -> LOGGER.info(k + ":" + v));

    }


    public Properties loadPropertiesFile(String filePath) {

        Properties dev = new Properties();

        try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filePath)) {
            dev.load(resourceAsStream);
        } catch (IOException e) {
            LOGGER.info("Unable to load properties file : " + filePath);
        }

        return dev;
    }
}