package com.scalefocus.controllers;

import com.scalefocus.services.GreetingService;
import com.scalefocus.services.GreetingServiceImpl;


public class PropertyInjectedController {


    public GreetingService greetingServiceImpl;

    public String sayHello() {
        return greetingServiceImpl.sayGreeting();
    }

    public void setGreetingServiceImpl(GreetingServiceImpl greetingServiceImpl) {
        this.greetingServiceImpl = greetingServiceImpl;
    }

}
