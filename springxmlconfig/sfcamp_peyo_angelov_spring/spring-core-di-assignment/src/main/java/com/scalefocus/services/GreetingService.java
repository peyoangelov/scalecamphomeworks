package com.scalefocus.services;

public interface GreetingService {

    String sayGreeting();
}
