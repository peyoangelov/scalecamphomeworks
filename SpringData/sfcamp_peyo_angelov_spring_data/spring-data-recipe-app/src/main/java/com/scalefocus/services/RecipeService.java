package com.scalefocus.services;

import com.scalefocus.domain.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();
}
